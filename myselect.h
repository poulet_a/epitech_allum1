/*
** myselect.h for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Fri Feb  7 14:03:39 2014 poulet_a
** Last update Tue Feb 11 10:32:39 2014 poulet_a
*/

#ifndef MYSELECT_H_
# define MYSELECT_H_

# define KEYBOARD_UP		4283163
# define KEYBOARD_DOWN		4348699
# define KEYBOARD_RIGHT		4414235
# define KEYBOARD_LEFT		4479771
# define KEYBOARD_RETURN	127
# define KEYBOARD_PLUS		43
# define KEYBOARD_LESS		45
# define KEYBOARD_ENTER		10
# define KEYBOARD_SUPPR		2117294875

int	my_select(int key, int lin_nb);
void	my_select_next(int *id, int size);
void	my_select_prev(int *id, int size);

#endif /* !MYSELECT_H_ */
