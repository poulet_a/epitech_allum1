/*
** myalu.h for 7 in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Fri Feb  7 14:03:15 2014 poulet_a
** Last update Sat Feb 15 11:14:55 2014 poulet_a
*/

#ifndef MYALU_H_
# define MYALU_H_

# define ALU_NB_LINES		6
# define ALU_SIZE_LAST_LINE	(ALU_NB_LINES - 1) * 2 + 1

# include "my.h"

int	alu_set_nb_lines(int n);
int	alu_get_size_last_line();
t_list	*alu_manage(t_list *alu);
int	alu_count(t_list *alu);
int	alu_put_one(t_list *alu, char current);
int	alu_prompt();
int	alu_rank_change(int i);
void	alu_rank_reset();
t_list	*alu_init(long col);
int	alu_get_e(t_list *alu);
int	alu_remove_id(t_list *alu, int id);

#endif /* !MYALU_H_ */
