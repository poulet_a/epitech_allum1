/*
** main.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Mon Feb  3 10:51:33 2014 poulet_a
** Last update Sat Feb 15 11:29:03 2014 poulet_a
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "mytermios.h"
#include "myselect.h"
#include "myalu.h"
#include "mygame.h"

int	my_get_key()
{
  int	buff;

  if (read(1, &buff, sizeof(int)) < 1)
    return (0);
  return (buff);
}

int		main_term_arround_the_loop(struct termios *t)
{
  static int	pos = 0;

  if (pos == 0)
    {
      RET_NULL_LONE(termios_save(t));
      RET_LONE_LONE(termios_canon(t));
      RET_LONE_LONE(termios_put_clear());
      RET_LONE_LONE(termios_cursor());
      RET_LONE_LONE(termios_display(t));
      pos = 1;
      alu_prompt(0);
    }
  else
    {
      RET_LONE_LONE(termios_display(t));
      RET_LONE_LONE(termios_cursor());
      RET_LONE_LONE(termios_canon(t));
      RET_NULL_LONE(termios_save(NULL));
      pos = 0;
    }
  return (0);
}

int			main(int ac, char **av, char **en)
{
  struct termios	*t;
  int			key;

  (ac == 2)?(alu_set_nb_lines(my_getnbr(av[1]))):(0);
  RET_NULL_LONE((t = termios_init(en, &my_get_env)));
  if (ac > 1 && my_match(av[1], "-h"))
    return (0 & my_putstr("Usage : ./allum1 [-h]\n"));
  RET_LONE_LONE(main_term_arround_the_loop(t));
  key = 0;
  while (game_play(0) != -1 && key == 0)
    {
      if ((key = my_get_key()))
	{
	  my_select(key, alu_set_nb_lines(0));
	  if (game_play(0) != -1)
	    RET_LONE_ZERO(alu_prompt());
	  key = 0;
	}
    }
  return (main_term_arround_the_loop(t));
}
